# Final Project Quivia

GameTech Quivia memiliki genre Trivia dan tentunya tujuan dari game ini adalah menjawab pertanyaan dengan benar sebanyak mungkin. 
5 pertanyaan yang disediakan seputar dunia industri game. Game ini bisa dimainkan sebanyak 2 pemain, pemain akan diberikan pertanyaan dan harus menjawab secepat mungkin karena siapa yang cepat menjawab dia yang mendapat poin. 
Satu pertanyaan benar dihitung 1 poin. Kecepetan dan pengetahuan pemain akan dilatih karena dalam menjawab setiap pertanyaan akan ada kompetisi secara real-time sehingga pemain dituntut untuk cepat dalam memainkannya. 
